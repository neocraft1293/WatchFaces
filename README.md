# Collection de Cadrans de Montre Personnalisés

Bienvenue dans la collection de cadrans de montre personnalisés ! Cette collection regroupe une variété de cadrans de montre uniques créés avec passion et attention aux détails.

## XeniaLinux par Rinidisc

Le premier cadran de cette collection est inspiré par XeniaLinux, créé par Rinidisc et publié sur Meow.social. Vous pouvez trouver l'image originale [ici](https://meow.social/@rinidisc/109713069555919700).

![xenialinux](https://codeberg.org/neocraft1293/WatchFaces/raw/branch/main/images/xenia.png)

## ArchChan par raviolimavioli

Le deuxième cadran de cette collection est inspiré par ArchChan. ArchChan est une personnification mignonne et emblématique d'Arch Linux.
Vous pouvez trouver l'image originale [ici](https://www.deviantart.com/raviolimavioli/art/Arch-chan-878404999).

![ArchChan](https://codeberg.org/neocraft1293/WatchFaces/raw/branch/main/images/ArchChan.png)

## Niko par Syphilis

Le troisième cadran de cette collection est inspiré par Niko. Niko est le personnage principal du jeu vidéo *OneShot*, un jeu d'aventure indépendant.

Vous pouvez trouver l'image originale [ici](https://www.zerochan.net/3697544).


![Niko](https://codeberg.org/neocraft1293/WatchFaces/raw/branch/main/images/niko.png)


## Utilisation de Watch Face Studio by Samsung

Tous les cadrans de cette collection ont été créés avec Watch Face Studio, un logiciel développé par Samsung. Watch Face Studio offre une variété d'outils pour concevoir et personnaliser des cadrans de montre uniques. Vous pouvez en savoir plus sur Watch Face Studio [ici](https://developer.samsung.com/watch-face-studio/overview.html) et télécharger le logiciel [ici](https://developer.samsung.com/watch-face-studio/download.html). Veuillez noter que le logiciel est compatible avec Windows.

## Exportation et Installation

### Exportation

Pour exporter un cadran de montre créé avec Watch Face Studio, vous devez le faire sous forme de fichier APK. Suivez ces étapes :

1. **Ouvrir Watch Face Studio :** Lancez Watch Face Studio sur votre ordinateur.
2. **Concevoir le cadran :** Créez ou modifiez votre cadran de montre selon vos préférences.
3. **Exporter :** Une fois satisfait de votre design, utilisez l'option d'exportation pour générer le fichier APK. Suivez les instructions fournies par Watch Face Studio pour terminer cette étape.

### Installation

Pour installer le fichier APK sur votre montre WearOS, vous devez mettre votre montre en mode développeur, activer le mode ADB sans fil, vous connecter à la montre et installer le fichier APK.

#### Mettre la montre en mode développeur

1. **Ouvrir les paramètres :** Allez dans les paramètres de votre montre.
2. **À propos de la montre :** Faites défiler jusqu'à "À propos de la montre" et appuyez dessus.
3. **Numéro de build :** Trouvez l'option "Numéro de build" et appuyez dessus sept fois. Vous verrez un message indiquant que vous êtes maintenant un développeur.

#### Activer le mode ADB sans fil

1. **Retour aux paramètres :** Retournez aux paramètres principaux de votre montre.
2. **Options pour les développeurs :** Allez dans les "Options pour les développeurs".
3. **ADB sans fil :** Activez "ADB sans fil". Notez l'adresse IP affichée.

#### Se connecter à la montre

1. **Ouvrir un terminal :** Sur votre ordinateur, ouvrez un terminal ou une invite de commandes.
2. **Connexion :** Tapez la commande suivante pour vous connecter à votre montre, en remplaçant `IP_de_votre_montre` par l'adresse IP affichée sur votre montre :

    ```bash
    adb connect IP_de_votre_montre
    ```

3. **Autoriser la connexion :** Sur votre montre, vous devrez autoriser la connexion ADB. Acceptez la demande de connexion.

#### Installer le fichier APK

1. **Installation :** Une fois connecté, utilisez la commande suivante pour installer le fichier APK sur votre montre, en remplaçant `chemin_vers_votre_fichier.apk` par le chemin d'accès complet vers le fichier APK :

    ```bash
    adb install chemin_vers_votre_fichier.apk
    ```

2. **Vérification :** Une fois l'installation terminée, vous verrez un message de confirmation. Vous pouvez maintenant ajouter le cadran à votre montre et profiter de votre création personnalisée !

---

Créé par Neocraft1293
